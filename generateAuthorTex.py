#!/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 27 Jan 2017

@author: ncl
'''
import argparse
import re

import yaml

from generateAuthorXML import getOptionalField
import importlib

namedTemplate = None
namedTemplateName = None

def latexify(inputText):
    inputText = inputText.replace("è", "\\`e")
    inputText = inputText.replace("é", "\\'e")
    inputText = inputText.replace("à", "\\`a")
    inputText = inputText.replace("ü", "\\\"u")
    inputText = inputText.replace("ä", "\\\"a")
    inputText = inputText.replace("í", "\\'i")
    inputText = inputText.replace("ó", "\\'o")
    inputText = inputText.replace("ö", "\\\"o")
    inputText = inputText.replace("&", "\\&")
    return inputText

def readTemplate():
    return

def readYamlFiles(supportingFile, authorsFile, institutesFile):
    with open(supportingFile, "r") as fd:
        supporting = yaml.full_load(fd)

    with open(authorsFile, "r") as fd:
        authors = yaml.full_load(fd)

    with open(institutesFile, "r") as fd:
        institutes = yaml.full_load(fd)

    for inst in institutes:
        if not "sorting1" in inst:
            inst["sorting1"] = "city"
        if not "sorting2" in inst:
            inst["sorting2"] = "country"

    return supporting, authors, institutes

def getSortedAuthorsForInstitute(authors, instituteRef):
    return sorted([author for author in authors if author["institute"]==instituteRef], key=lambda k: k["name"])

def orderFun(footNotes, valList):
    return sorted(valList, key = lambda k: footNotes[k] if k in footNotes else 1000)

def extendElement(dico, el1, el2):
    """
    Add el2 into el1 of dico
    """
    if not el2 in dico:
        return # Nothing to add
    if not el1 in dico:
        dico[el1] = []

    if type(dico[el1])!=list:
        dico[el1] = [dico[el1]]

    if type(dico[el2])!=list:
        dico[el2] = [dico[el2]]

    dico[el1].extend(dico[el2])

def generateAuthorMetadata(author, supportingText, correspondingAuthors, sortedSupporting):
    # Compute abbreviated surname
    if not "abbsurname" in author:
        author["abbsurname"] = "{}.".format(author["surname"][0])

    # Compute status
    author["status"] = getOptionalField(author, None, "status", "", "", namedTemplate.statusTemplate, -1, "")
    author["orcid"] = getOptionalField(author, None, "orcidid", "", "", namedTemplate.orcidTemplate, -1, "")

    # Merge funding with connection... no difference in latex
    extendElement(author, "connection", "funding")

    if not correspondingAuthors is None and author["name"] in correspondingAuthors:
        author["corresponding"] = namedTemplate.correspondingTemplate
    else:
        author["corresponding"] = ""

    # Compute also at and present address (connection)
    author["otherconnections"] = getOptionalField(author, None, "connection", "", "", namedTemplate.connectionTemplate, -1, "", (orderFun, sortedSupporting), joint="$^,$")
    if hasattr(namedTemplate, "instituteTemplate"):
        author["fninstitute"] = getOptionalField(author, None, "institute", "", "", namedTemplate.instituteTemplate, -1, "")


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def generateGroupedAuthorList(authors, institutes, supportingText, correspondingAuthors, sorting):
    if "NA62" in namedTemplateName:
        sortedInstitute = sorted(institutes, key=lambda k: ("zzzz" if k[k[sorting]] is None else k[k[sorting]].lower(), k["city"], k["name"]))
    else:
        sortedInstitute = sorted(institutes, key=lambda k: "zzzz" if k[k[sorting]] is None else k[k[sorting]].lower())

    listGroups = []
    sortedSupporting = {}
    deceased = False
    for inst in sortedInstitute:
        sortedAuthors = getSortedAuthorsForInstitute(authors, inst["id"])
        aList = []
        for author in sortedAuthors:
            generateAuthorMetadata(author, supportingText, correspondingAuthors, sortedSupporting)

            if not author["status"] is None:
                deceased = True
            if not author["otherconnections"] is None:
                m = re.finditer("\$([\w-]+)", author["otherconnections"])
                for match in m:
                    if not match.group(1) in sortedSupporting:
                        sortedSupporting[match.group(1)] = len(sortedSupporting) + 1

            notes = [author["corresponding"], author["status"], author["otherconnections"]]
            notes = [el for el in notes if len(el) > 0]
            aList.append(" {abbsurname}~{name}{footnotes}{orcid}".format(footnotes = "$^,$".join(notes), **author))

        if len(aList) == 0:
            continue
        template_dict = {"institute": inst["name"],
                         "address": inst["address"],
                         "city": inst["city"],
                         "country": inst["country"]
                         }
        if len(aList) > 6 and namedTemplateName != "NA62":
            template_dict["authors"] = ", \\\\\n".join([",\n".join(x) for x in chunks(aList, 5)])
        else:
            template_dict["authors"] = ",\n".join(aList)
        if inst["address"] is None:
            template_to_use = namedTemplate.groupTemplateNoAddress
        else:
            template_to_use = namedTemplate.groupTemplate
        listGroups.append(template_to_use.format(**template_dict))

    sortedInstitute = {k["id"]: i for i, k in enumerate(sortedInstitute)}
    return sortedInstitute, sortedSupporting, deceased, "\n".join(listGroups)


def generateAlphabeticalAuthorList(authors, institutes, supportingText, correspondingAuthors, sorting):
    sortedAuthors = sorted(authors, key = lambda k: k["name"].lower())

    listGroups = []
    aList = []
    sortedSupporting = {}
    usedInstitutes = []
    deceased = False
    for author in sortedAuthors:
        generateAuthorMetadata(author, supportingText, correspondingAuthors, sortedSupporting)

        if not author["status"] is None:
            deceased = True
        if not author["otherconnections"] is None:
            m = re.finditer("\$([\w-]+)", author["otherconnections"])
            for match in m:
                if not match.group(1) in sortedSupporting:
                    sortedSupporting[match.group(1)] = len(sortedSupporting) + 1

        usedInstitutes.append(author["institute"])

        notes = [author["fninstitute"], author["corresponding"], author["status"], author["otherconnections"]]
        notes = [el for el in notes if len(el) > 0]
        aList.append(" {abbsurname}~{name}{footnotes}".format(footnotes = "$^,$".join(notes), **author))

    template_dict = {"institute": "institute",
                    "address": "address",
                    "city": "city",
                    "country": "country",
                    "authors": ",\n".join(aList)
                    }
    # if len(aList) > 6 and namedTemplateName != "NA62":
    #    groupT.authors = ", \\\\\n".join([",\n".join(x) for x in chunks(aList, 5)])
    # else:
    listGroups.append(namedTemplate.groupTemplate.format(**template_dict))

    if "NA62" in namedTemplateName:
        sortedInstitute = sorted(institutes, key = lambda k: ("zzzz" if k[k[sorting]] is None else k[k[sorting]].lower(), k["city"], k["name"]))
    else:
        sortedInstitute = sorted(institutes, key = lambda k: "zzzz" if k[k[sorting]] is None else k[k[sorting]].lower())

    sortedInstitute = {k["id"]: i for i, k in enumerate([_ for _ in sortedInstitute if _["id"] in usedInstitutes])}

    return sortedInstitute, sortedSupporting, deceased, "\n".join(listGroups)


def findEmails(aList, allAuthors):
    emails = []
    for author in allAuthors:
        if author["name"] in aList:
            if "email" in author:
                emails.append(author["email"])
            else:
                emails.append("{0} no mail".format(author["name"]))

    return emails


def generateFootnotes(supportingText, institutes, authors, sortedSupporting, sortedInstitutes, doDeceased, correspondingAuthors):
    symFn_dict = None
    if doDeceased or len(correspondingAuthors)>0:
        symFn_dict = {"deceased": "",
                      "corresponding": ""}
        if doDeceased:
            symFn_dict["deceased"] = namedTemplate.deceasedTemplate
        if not correspondingAuthors is None and len(correspondingAuthors)>0:
            emails = findEmails(correspondingAuthors, authors)
            symFn_dict["corresponding"] = namedTemplate.correspondingTemplateFN.format(names=", ".join(correspondingAuthors), emails=", ".join(emails))

    invertedSort = {v: k for k,v in sortedSupporting.items()}

    listSupporting = []
    institutesDict = {el["id"]: el for el in institutes}

    for number, supporting in invertedSort.items():
        fnT_dict = {"number": number,
                    "value": ""}
        if supporting in supportingText:
            if supportingText[supporting]["type"]=="funding":
                fnT_dict["value"]=supportingText[supporting]["value"]
            else:
                if supportingText[supporting]["type"]=="Present address":
                    typeText = "{}:".format(supportingText[supporting]["type"])
                else:
                    typeText = supportingText[supporting]["type"]
                fnT_dict["value"]="{type} {name}, {address}".format(type=typeText, **institutesDict[supportingText[supporting]["id"]])
        listSupporting.append(namedTemplate.numberFnTemplate.format(**fnT_dict))

    listInstitutes = []
    if hasattr(namedTemplate, "instFnTemplate"):
        for institute, number in sorted(sortedInstitutes.items(), key = lambda k: k[1]):
            fnT_dict = {"number": number + 1,
                        "value": ""}
            if institute in institutesDict:
                fnT_dict["value"] = "{name}, {address}".format(**institutesDict[institute])
            listInstitutes.append(namedTemplate.instFnTemplate.format(**fnT_dict))

    if namedTemplateName == "NA62_PRL":
        symText = ""
        if symFn_dict is None:
            symFn_dict = {"deceased": "",
                          "corresponding": ""}
        symFn_dict["connection"] = "\n".join(listSupporting)
        symText = namedTemplate.symbolFnTemplate.format(**symFn_dict)
        return symText, "\n".join(listInstitutes)
    else:
        symText = ""
        if not symFn_dict is None:
            symText = namedTemplate.symbolFnTemplate.format(**symFn_dict)
        return symText, "\n".join(listSupporting)


def replaceReference(sortedSupporting, institutes, groupsText, doDeceased, doCorresponding):
    t_dict = {}

    for support in sortedSupporting:
        t_dict[support] = sortedSupporting[support]

    for institute in institutes:
        t_dict[institute] = institutes[institute] + 1

    if doDeceased:
        t_dict["deceased"] = 2

    if doCorresponding:
        t_dict["corresponding"] = 1

    for v in t_dict:
        groupsText = groupsText.replace(f"${v}", str(t_dict[v]))

    return groupsText


def replaceTex(matchObj):
    return matchObj.group(1)


if __name__ == '__main__':

    import pkgutil
    import TexTemplates

    parser = argparse.ArgumentParser(description='Generates the LaTeX author list for NA62')
    parser.add_argument('templateName', metavar='templateName', type=str, choices=[mod[1] for mod in pkgutil.iter_modules(TexTemplates.__path__)])
    parser.add_argument('supportingFile', metavar='supportingFile', type=str)
    parser.add_argument('authorsFile', metavar='authorsFile', type=str)
    parser.add_argument('instituteFile', metavar='instituteFile', type=str)
    parser.add_argument('collaborationName', metavar='collaborationName', type=str)
    parser.add_argument('--corresponding', dest='correspondingAuthors', action='append')
    parser.add_argument('--sorting', dest='sorting', type=int, choices=[1,2], default=1)

    args = parser.parse_args()

    namedTemplate = importlib.import_module(".{0}".format(args.templateName), package="TexTemplates")
    namedTemplateName = args.templateName
    valueDict = {}

    supportingText, authors, institutes = readYamlFiles(args.supportingFile, args.authorsFile, args.instituteFile)

    texTemplate_dict = {"collaborationName": args.collaborationName}
    if "PRL" in namedTemplateName:
        sortedInstitutes, sortedSupporting, deceased, groupsText = generateAlphabeticalAuthorList(authors, institutes, supportingText, args.correspondingAuthors, "sorting{0}".format(args.sorting))
    else:
        sortedInstitutes, sortedSupporting, deceased, groupsText = generateGroupedAuthorList(authors, institutes, supportingText, args.correspondingAuthors, "sorting{0}".format(args.sorting))
    symbolFootnoteText, footnoteText = generateFootnotes(supportingText, institutes, authors, sortedSupporting, sortedInstitutes, deceased, args.correspondingAuthors)

    groupsText = replaceReference(sortedSupporting, sortedInstitutes, groupsText, deceased, True)
    texTemplate_dict["authorListByInstitute"] = re.sub("\\\\tex\{(.*?)\}", replaceTex, groupsText)
    texTemplate_dict["numberedFootnote"] = re.sub("\\\\tex\{(.*?)\}", replaceTex, footnoteText)
    texTemplate_dict["symbolFootnote"] = re.sub("\\\\tex\{(.*?)\}", replaceTex, symbolFootnoteText)

    print(latexify(namedTemplate.texsrcTemplate.format(**texTemplate_dict)))


