'''
Created on 13 Feb 2017

@author: ncl
'''

texsrcTemplate = """
\\newcommand{{\\orcimg}}{{\\raisebox{{-0.3\\height}}{{\\includegraphics[height=\\fontcharht\\font`A]{{ORCIDiD_iconvector}}}}}}
\\newcommand{{\\orcid}}[1]{{\\href{{https://orcid.org/#1}}{{\\orcimg}}}}

{{\\bf \\noindent The {collaborationName} Collaboration}}
\\vspace{{1cm}}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

\\begin{{raggedright}}
\\noindent
{authorListByInstitute}
\\end{{raggedright}}

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

\setcounter{{footnote}}{{0}}
\\newlength{{\\basefootnotesep}}
\setlength{{\\basefootnotesep}}{{\\footnotesep}}

{symbolFootnote}
{numberedFootnote}
"""

groupTemplate="""%%%%%%%
{{\\bf {institute}, {city}, {country}}}\\\\
{authors}
\\vspace{{0.5cm}}
"""

groupTemplateNoAddress="""%%%%%%%
{{\\bf {institute}}}\\\\
{authors}
\\vspace{{0.5cm}}
"""

numberFnTemplate="""$^{{{number}}}${{{value}}} \\\\"""

instFnTemplate = """$^{{{number}}}${{\centering {value}}} \\\\"""

symbolFnTemplate = """\\renewcommand{{\\thefootnote}}{{\\fnsymbol{{footnote}}}}
\\noindent
{corresponding}
{deceased}
\\renewcommand{{\\thefootnote}}{{\\arabic{{footnote}}}}"""


statusTemplate          = "$\,$\\renewcommand{{\\thefootnote}}{{\\fnsymbol{{footnote}}}}\\footnotemark[$deceased]\\renewcommand{{\\thefootnote}}{{\\arabic{{footnote}}}}"
correspondingTemplate   = "$\,$\\renewcommand{\\thefootnote}{\\fnsymbol{footnote}}\\footnotemark[$corresponding]\\renewcommand{\\thefootnote}{\\arabic{footnote}}"
connectionTemplate      = "$\,${{\\footnotemark[${value}]}}"
instituteTemplate       = "\\footnotemark[${value}]"
deceasedTemplate        = "$^{\\footnotemark[2]}${Deceased}\\\\"
orcidTemplate           = "\\orcid{{{value}}}"
correspondingTemplateFN = "$^{{\\footnotemark[1]}}${{Corresponding author: {names}, email: {emails}}}\\\\"
