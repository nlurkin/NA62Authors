'''
Created on 13 Feb 2017

@author: ncl
'''

texsrcTemplate = """{authorListByInstitute}

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\setcounter{{footnote}}{{0}}
{symbolFootnote}

{numberedFootnote}"""

groupTemplate="""%%%%%%%
{{\\bf {institute}, {address}}}\\\\
{authors}\\\\[2mm]
"""

groupTemplateNoAddress="""%%%%%%%
{{\\bf {institute}}}\\\\
{authors}\\\\[2mm]
"""

numberFnTemplate="""%
\\footnotetext[{number}]{{{value}}}"""

symbolFnTemplate="""\\renewcommand{{\\thefootnote}}{{\\fnsymbol{{footnote}}}}
{corresponding}
{deceased}
\\renewcommand{{\\thefootnote}}{{\\arabic{{footnote}}}}
"""


statusTemplate          = "$\,$\\renewcommand{{\\thefootnote}}{{\\fnsymbol{{footnote}}}}\\footnotemark[$deceased]\\renewcommand{{\\thefootnote}}{{\\arabic{{footnote}}}}"
correspondingTemplate   = "$\\renewcommand{\\thefootnote}{\\fnsymbol{footnote}}\\footnotemark[$corresponding]\\renewcommand{\\thefootnote}{\\arabic{footnote}}$"
connectionTemplate      = "$\,$\\footnotemark[${value}]"
deceasedTemplate        = "\\footnotetext[2]{Deceased}"
orcidTemplate           = ""
correspondingTemplateFN = "\\footnotetext[1]{{Corresponding author: {names}, email: {emails}}}"
