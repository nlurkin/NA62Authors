'''
Created on 13 Feb 2017

@author: ncl
'''

texsrcTemplate = """
\\begin{{center}}
{collaborationName} Collaboration
\end{{center}}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

{authorListByInstitute}

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%

\setcounter{{footnote}}{{0}}
\\newlength{{\\basefootnotesep}}
\setlength{{\\basefootnotesep}}{{\\footnotesep}}

\\begin{{center}}
{numberedFootnote}
\\end{{center}}
{symbolFootnote}
"""

groupTemplate="""%%%%%%%
\\begin{{center}}
{authors}
\end{{center}}
"""

groupTemplateNoAddress=groupTemplate

numberFnTemplate="""%
$^{{\\alphalph{{{number}}}}}${{{value}}} \\\\"""

instFnTemplate = """%
$^{{{number}}}${{\centering {value}}} \\\\"""

symbolFnTemplate = """\\vspace{{10pt}}
\\renewcommand{{\\thefootnote}}{{\\fnsymbol{{footnote}}}}
{corresponding}
{deceased}
\\vspace{{10pt}} \\\\
\\renewcommand{{\\thefootnote}}{{\\alphalph{{\\value{{footnote}}}}}}
{connection}
\\renewcommand{{\\thefootnote}}{{\\arabic{{footnote}}}}
"""


statusTemplate          = "$\,$\\renewcommand{{\\thefootnote}}{{\\fnsymbol{{footnote}}}}\\footnotemark[$deceased]\\renewcommand{{\\thefootnote}}{{\\arabic{{footnote}}}}"
correspondingTemplate   = "$\,$\\renewcommand{\\thefootnote}{\\fnsymbol{footnote}}\\footnotemark[$corresponding]\\renewcommand{\\thefootnote}{\\arabic{footnote}}"
connectionTemplate      = "$\,$\\renewcommand{{\\thefootnote}}{{\\alphalph{{\\value{{footnote}}}}}}\\footnotemark[${value}]\\renewcommand{{\\thefootnote}}{{\\arabic{{footnote}}}}"
instituteTemplate       = "\\footnotemark[${value}]"
deceasedTemplate        = "$^{\\footnotemark[2]}${Deceased}"
orcidTemplate           = ""
correspondingTemplateFN = "$^{{\\footnotemark[1]}}${{Corresponding author: {names}, email: {emails}}}\\\\"
