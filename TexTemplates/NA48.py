'''
Created on 13 Feb 2017

@author: ncl
'''


texsrcTemplate="""\begin{{center}}
{{\Large The {collaborationName} collaboration}}\\
\vspace{{2mm}}


{authorListByInstitute}


\end{{center}}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
{symbolFootnote}

{numberedFootnote}"""

groupTemplate="""%
{authors}\\\\
{{\em \small {institute}, {address}}} \\\\[0.2cm]
%
"""

groupTemplateNoAddress="""%
{authors}\\\\
{{\em \small {institute}}} \\\\[0.2cm]
%
"""

numberFnTemplate="""%
\\footnotetext[{number}]{{{value}}}"""

symbolFnTemplate="""\\renewcommand{{\\thefootnote}}{{\\fnsymbol{{footnote}}}}
{corresponding}
{deceased}
\\renewcommand{{\\thefootnote}}{{\\arabic{{footnote}}}}
"""


statusTemplate          = "$\,$\\renewcommand{{{{\\thefootnote}}}}{{{{\\fnsymbol{{{{footnote}}}}}}}}\\footnotemark[$deceased]\\renewcommand{{{{\\thefootnote}}}}{{{{\\arabic{{{{footnote}}}}}}}}"
correspondingTemplate   = "$\\renewcommand{{\\thefootnote}}{{\\fnsymbol{{footnote}}}}\\footnotemark[$corresponding]\\renewcommand{{\\thefootnote}}{{\\arabic{{footnote}}}}$"
connectionTemplate      = "$\,$\\footnotemark[${{value}}]"
deceasedTemplate        = "\\footnotetext[2]{{Deceased}}"
orcidTemplate           = ""
correspondingTemplateFN = "\\footnotetext[1]{{{{Corresponding author, email: {{emails}}}}}}   %{{names}}"