#!/bin/env python

'''
Created on 27 Jan 2017

@author: ncl
'''
import argparse
from datetime import date, datetime
import re

import yaml


xmlTemplate = """<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE collaborationauthorlist SYSTEM "author.dtd">

<collaborationauthorlist
   xmlns:foaf="http://xmlns.com/foaf/0.1/"
   xmlns:cal="http://www.slac.stanford.edu/spires/hepnames/authors_xml/">

   <cal:creationDate>{creationDate}</cal:creationDate>
   <cal:publicationReference>{publication}</cal:publicationReference>


   <cal:collaborations>
      <cal:collaboration id="c1">
         <foaf:name>{collaborationName}</foaf:name>
      </cal:collaboration>
   </cal:collaborations>


   <cal:organizations>

      {listInstitutions}

   </cal:organizations>

    <cal:authors>

      {listAuthors}

   </cal:authors>

</collaborationauthorlist>
"""

instituteTemplate = """
      <foaf:Organization id="{id}">
         <foaf:name>{name}</foaf:name>
         <cal:orgAddress>{name}, {address}</cal:orgAddress>{inspirestring}
      </foaf:Organization>"""

authorTemplate = """
      <foaf:Person>
         <foaf:name>{surname} {name}</foaf:name>
         <foaf:givenName>{surname}</foaf:givenName>
         <foaf:familyName>{name}</foaf:familyName>{status}
         <cal:authorNamePaper>{abbsurname} {name}</cal:authorNamePaper>
         <cal:authorNamePaperGiven>{abbsurname}</cal:authorNamePaperGiven>
         <cal:authorNamePaperFamily>{name}</cal:authorNamePaperFamily>
         <cal:authorCollaboration collaborationid="c1"/>
         <cal:authorAffiliations>
            <cal:authorAffiliation organizationid="{institute}"/>{otherConnections}
         </cal:authorAffiliations>{authIDsstring}{funding}
      </foaf:Person>"""

authIDsTemplate = """<cal:authorids>{inspirestring}{orcidstring}
         </cal:authorids>"""

def xmlify(inputText):
    inputText = inputText.replace("&", "&amp;")
    return inputText


def unnaccent(dico):
    return
    for el, val in dico.items():
        if not dico[el] is None and type(val) == str:
            dico[el] = dico[el].replace("\xe9", "e")
            dico[el] = dico[el].replace("\xe8", "e")
            dico[el] = dico[el].replace("\xfc", "u")
            dico[el] = dico[el].replace("\xe0", "a")
            dico[el] = dico[el].replace("\xe4", "a")
            dico[el] = dico[el].replace("\xed", "i")
            dico[el] = dico[el].replace("\xf3", "o")


def readYamlFiles(supportingFile, authorsFile, institutesFile):
    with open(supportingFile, "r") as fd:
        supporting = yaml.full_load(fd)
    for el in supporting.values():
        unnaccent(el)

    with open(authorsFile, "r") as fd:
        authors = yaml.full_load(fd)
    for el in authors:
        unnaccent(el)

    with open(institutesFile, "r") as fd:
        institutes = yaml.full_load(fd)
    for el in institutes:
        unnaccent(el)

    return supporting, authors, institutes


def countAuthorsForInstitute(institute, authors):
    return  len([a for a in authors if a["institute"] == institute["id"]])

def countAuthorsForSupporting(authors, support):
    return len([a for a in authors if "connection" in a and a["connection"] == support])

def countSupportingForInstitute(institute, authors, supporting):
    return len([support for support in supporting
                if supporting[support]["id"]==institute["id"]
                and countAuthorsForSupporting(authors, support) > 0
                ])

def generateInstitutes(institutes, authors, supporting):
    totalString = ""
    for institute in institutes:
        if countAuthorsForInstitute(institute, authors) > 0 or countSupportingForInstitute(institute, authors, supporting) > 0:
            unnaccent(institute)
            institute["inspirestring"] = getOptionalField(institute, None, "inspire", "", "", "<cal:orgName source=\"INSPIRE\">{value}</cal:orgName>", 9)
            totalString += instituteTemplate.format(**institute)
    return totalString


def getOptionalField(author, valueDict, fieldName, valIfNone, valIfEmpty, valIfExists, nspaces, linestart="\n", orderFunction=None, joint=""):
    if fieldName in author:
        if author[fieldName] is None:
                returnValue = "" if valIfEmpty=="" else "{0:<{fill}}{valString}".format(linestart, fill=nspaces + 1, valString=valIfEmpty)
        else:
            valList = author[fieldName]
            if type(author[fieldName]) != list:
                valList = [author[fieldName]]
            returnValue = []
            if not orderFunction is None:
                valList = orderFunction[0](orderFunction[1], valList)
            for val in valList:
                if valueDict == None:
                    returnValue.append("{0:<{fill}}{valString}".format(linestart, fill=nspaces + 1, valString=valIfExists).format(value=val))
                else:
                    returnValue.append("{0:<{fill}}{valString}".format(linestart, fill=nspaces + 1, valString=valIfExists).format(**valueDict[val]))
            returnValue = joint.join(returnValue)
    else:
        returnValue = valIfNone

    return returnValue


def generateAuthorMetadata(author, supportingText):
    # Compute abbreviated surname
    if not "abbsurname" in author:
        author["abbsurname"] = "{}.".format(author["surname"][0])

    # Compute status
    author["status"] = getOptionalField(author, None, "status", "", "<cal:authorStatus />", "<cal:authorStatus>{value}</cal:authorStatus>", 9)

    # Compute funding
    author["funding"] = getOptionalField(author, supportingText, "funding", "", "<cal:authorFunding />", "<cal:authorFunding>{value}</cal:authorFunding>", 9)

    # Compute also at and present address (connection)
    author["otherConnections"] = getOptionalField(author, supportingText, "connection", "", "", "<cal:authorAffiliation organizationid=\"{id}\" connection=\"{type}\"/>", 12)

    # Compute presence of orcidid
    author["orcidstring"] = getOptionalField(author, None, "orcidid", "", "", "<cal:authorid source=\"ORCID\">{value}</cal:authorid>", 12)

    # Compute presence of inspireid
    author["inspirestring"] = getOptionalField(author, None, "inspireid", "", "", "<cal:authorid source=\"INSPIRE\">{value}</cal:authorid>", 12)

    author["authIDsstring"] = ""
    if "orcidid" in author or "inspireid" in author:
        author["authIDsstring"] = authIDsTemplate.format(**author)

def generateAuthors(authors, supportingText):
    totalString = ""
    for author in authors:
        generateAuthorMetadata(author, supportingText)

        unnaccent(author)
        totalString += authorTemplate.format(**author)
    return totalString


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generates the XML author list for NA62 required to be uploaded with an arXiv publications for accurate referencing by INSPIRE.')
    parser.add_argument('supportingFile', metavar='supportingFile', type=str)
    parser.add_argument('authorsFile', metavar='authorsFile', type=str)
    parser.add_argument('instituteFile', metavar='instituteFile', type=str)
    parser.add_argument('collaborationName', metavar='collaborationName', type=str)
    parser.add_argument('publicationID', metavar='publicationID', type=str)

    args = parser.parse_args()

    valueDict = {}

    supportingText, authors, institutes = readYamlFiles(args.supportingFile, args.authorsFile, args.instituteFile)

    valueDict["listAuthors"] = generateAuthors(authors, supportingText)
    valueDict["listInstitutions"] = re.sub("\\\\tex\{.*\}", "", generateInstitutes(institutes, authors, supportingText))
    valueDict["collaborationName"] = args.collaborationName
    valueDict["publication"] = args.publicationID
    valueDict["creationDate"] = date.strftime(datetime.now(), "%Y-%m-%d_%H:%M")

    print(xmlify(xmlTemplate.format(**valueDict)))

